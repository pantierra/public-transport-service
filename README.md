Public Transport Service
========================

Web service API around the [Public Transport Enabler](https://github.com/schildbach/public-transport-enabler) Java library to get data from public transport providers.

Install
-------

First, install the dependend library, for this, clone the [public-transport-enabler](https://github.com/schildbach/public-transport-enabler) repository and run inside:

    $ gradle install

Then, inside the public-transport-service directory, copy the example configuration file, open it and define the provider to use:

    $ cp example.config.json config.json

Afterwards, you can build the web service using Gradle:

    $ gradle clean build

Run
---

Once, it is ready it can be started with:

    $ java -jar build/libs/public-transport-service.jar

And the API will be reachable under `localhost:8080`

Contribute
----------

This is unfinished work in progress and your input, thoughs, comments and contributions are very welcome. Please open issues and propose Merge Requests to the code.

Copy it
-------

Free and Open Source Software published under the [AGPL version 3](./COPYING) license.
